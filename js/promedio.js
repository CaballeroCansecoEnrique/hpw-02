var Promedio = (function imprimirpromedio()
{
  var tabla_calificaciones=document.getElementById('tabla_calificaciones');

  var cuerpo_tabla =tabla_calificaciones.children[1];

  var total=0;

for (var i=0; i<cuerpo_tabla.children.length; i++)
{
 total += Number(cuerpo_tabla.children[i].children[2].textContent);
}

  var promedio=total  / cuerpo_tabla.children.length;
  return promedio;
  
});

parrafo_promedio.textContent= 'Promedio: ' + Promedio();


var CalificacionMayor = (function calculaMayor()
{
  var tabla_calificacionesmayor=document.getElementById('tabla_calificaciones');
  var cuerpo_tabla =tabla_calificacionesmayor.children[1];

  var compara1;
  var compara2 = 0;

for (var i=0; i<cuerpo_tabla.children.length; i++) {
 compara1 = parseInt(cuerpo_tabla.children[i].children[2].textContent);
 if (compara1 > compara2){
     var mayor = compara1;
     compara2 = compara1;
     materia = (cuerpo_tabla.children[i].children[1].textContent);
     clave = (cuerpo_tabla.children[i].children[0].textContent);
 }
}
return clave+" -" + materia +" -" +mayor;
});

parrafo_mayor.textContent= 'Calificacion_Mayor: ' + CalificacionMayor();
 
 
var CalificacionMenor = (function calculaMenor()
{
  var tabla_calificaciones=document.getElementById('tabla_calificaciones');
  var cuerpo_tabla =tabla_calificaciones.children[1];

  var compara1;
  var compara2 = 100;

for (var i=0; i<cuerpo_tabla.children.length; i++) {
 compara1 = parseInt(cuerpo_tabla.children[i].children[2].textContent);
 
 if (compara1 < compara2){
     var menor = compara1;
     compara2 = compara1;
     materia = (cuerpo_tabla.children[i].children[1].textContent);
     clave = (cuerpo_tabla.children[i].children[0].textContent);
 }
}
return  clave+" -" + materia +" -" +menor;
});

parrafo_menor.textContent= 'Calificacion_Menor: ' + CalificacionMenor();
